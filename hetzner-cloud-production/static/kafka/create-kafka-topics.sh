# if-not-exists works with the --zookeeper option but not the --bootstrap-server yet.

/usr/local/kafka/bin/kafka-topics.sh  --zookeeper localhost:2181 --create --if-not-exists --topic hetzner-event-input --partitions 10 --replication-factor 3 --config retention.ms=604800000
/usr/local/kafka/bin/kafka-topics.sh  --zookeeper localhost:2181 --create --if-not-exists --topic hetzner-bus-event-output --partitions 1 --replication-factor 3 --config retention.ms=604800000
/usr/local/kafka/bin/kafka-topics.sh  --zookeeper localhost:2181 --create --if-not-exists --topic hetzner-status --partitions 1 --replication-factor 3 --config retention.ms=604800000
/usr/local/kafka/bin/kafka-topics.sh  --zookeeper localhost:2181 --create --if-not-exists --topic hetzner-percolator-input --partitions 200 --replication-factor 3 --config retention.ms=604800000