# When run via SSH, this will add a UFW firewall exception to allow access from the machine SSHing in.
# Allows other ports, to be available to the user running the deploy scripts, e.g. docker's :2376
`echo $SSH_CLIENT | awk '{print "sudo ufw allow from " $1 }'`