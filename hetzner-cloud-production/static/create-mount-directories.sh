#!/usr/bin/env bash

# Create directories that need to exist on every Docker host 
# so they can be volumed-mapped to containers.

directories=(
	/var/local/container-data/haproxy
	/var/local/container-data/redis
)


for i in "${directories[@]}"
do
  if [ ! -d "$i" ]; then
    echo "Create $i"
    sudo mkdir -p $i
    sudo chmod a+rwx -R $i
  fi
done


