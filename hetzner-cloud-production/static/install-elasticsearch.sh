# Script for installing ElasticSearch
# depends on:
# - provision.sh to mount an EBS volume
# - deploy directory having been copied to to /mnt/persistent/deploy

# https://www.elastic.co/downloads/elasticsearch#ga-release

set -e

if [ -d /etc/elasticsearch ]; then
  echo "Elastic Already Installed!"
else

  # If this runs on first startup, /var/lib/apt may be locked from first login. 
  # Let this pass.
  sleep 60
  sudo apt-get update


  # 1.7 comes preinstalled. Elastic wants 1.8.
  # sudo yum remove java-1.7.0-openjdk -y
  sudo apt-get update
  sudo apt install openjdk-8-jre apt-transport-https wget nginx -y

  # wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
  # echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list
  # sudo apt-get update && sudo apt-get install elasticsearch-oss
  cd /tmp  
  wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-oss-6.3.0.deb
  sudo dpkg -i elasticsearch-oss-6.3.0.deb

  sudo update-rc.d elasticsearch defaults 95 10

  # Link the config files from those copied in by terraform.
  sudo rm /etc/elasticsearch/elasticsearch.yml || true
  sudo ln -s /var/local/deploy/templated/elasticsearch.yml /etc/elasticsearch/elasticsearch.yml
  sudo chown -h elasticsearch:elasticsearch /etc/elasticsearch/elasticsearch.yml

  sudo rm /etc/elasticsearch/jvm.options
  sudo ln -s /var/local/deploy/static/elasticsearch/jvm.options /etc/elasticsearch/jvm.options
  sudo chown -h elasticsearch:elasticsearch /etc/elasticsearch/jvm.options

  sudo mkdir -p /var/data/elasticsearch
  sudo chown elasticsearch:elasticsearch /var/data/elasticsearch

  sudo mkdir -p /var/log/elasticsearch
  sudo chown elasticsearch:elasticsearch /var/log/elasticsearch

  # First time the 'Stopping ElasticSearch' will "FAIL" because it wasn't running. 
  # Ignore this, as it wasn't really meant to be first time round. 
  # Using restart here ensures we can re-run the script.
  sudo service elasticsearch restart
  echo "Finished installing Elasticsearch"
fi