# From https://docs.docker.com/install/linux/docker-ce/ubuntu/

if which docker
then
  echo "Docker is Already installed, skipping install";
  exit 0
fi

set -e 

# If this runs on first startup, /var/lib/apt may be locked from first login. 
# Let this pass.
sleep 20
sudo apt-get update

sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common -y

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Packaged for the previous version for now. It works fine.
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu artful stable"

sudo apt-get update

sudo apt-get install docker-ce=18.03.1~ce-0~ubuntu -y

sudo mkdir -p /etc/systemd/system/docker.service.d/

echo vm.max_map_count = 262144 | sudo tee -a /etc/sysctl.conf

sudo systemctl daemon-reload

sudo service docker restart

# Hello world for the exit code to show that it's installed OK.
sudo docker run hello-world


# inetutils-syslogd