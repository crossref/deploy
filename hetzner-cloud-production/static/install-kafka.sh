#!/usr/bin/env bash

sudo apt-get update

echo "Starting Kafka install script..."

set -e

if [ -d /var/kafka/logs ] ; then
  echo "Already installed Kafka"
else
  # Kafka sadly isn't available via APT.
  cd /tmp
  wget http://apache.mirror.anlx.net/kafka/2.2.1/kafka_2.12-2.2.1.tgz
  tar xvzf kafka_2.12-2.2.1.tgz
  
  sudo rm -rf /usr/local/kafka
  sudo mv kafka_2.12-2.2.1 /usr/local/kafka
  sudo chown deploy /usr/local/kafka
  
  # Kafa data directory.
  sudo mkdir -p /var/kafka/logs
  sudo chown deploy /var/kafka/logs

  # Link the appropriate templated Kafka config file.
  # The linked-to file may be updated in future. 
  # Exists in installation so need to remove it.
  sudo rm -f /usr/local/kafka/config/server.properties
  ln -s /var/local/deploy-dynamic/kafka/server.properties /usr/local/kafka/config/server.properties
  
  
  # Install the service.
  sudo cp /var/local/deploy/static/kafka/kafka.service /etc/systemd/system/kafka.service
  sudo systemctl daemon-reload

  echo "Starting Kafka..."
  sudo systemctl start kafka.service
  echo "Kafka running!"
fi



