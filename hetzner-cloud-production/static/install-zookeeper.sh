#!/usr/bin/env bash

echo "Starting ZooKeeper install script..."

set -e

echo "Installing ZooKeeper?"
if ( -d /var/zookeeper/data ) ; then 
  echo "Already installed Zookeeper"
else
  echo "Installing Zookeeper"
  sudo apt-get update
  sudo apt-get install openjdk-8-jre-headless -y

  sudo apt-get install zookeeperd -y


  sudo mkdir -p /var/zookeeper/data
  sudo chmod 777 /var/zookeeper/data

  # These are placed here prior to installer running.
  # Symlink so they can be updated.

  sudo rm -f /etc/zookeeper/conf/zoo.cfg
  sudo ln -s /var/local/deploy-dynamic/kafka/zoo.cfg /etc/zookeeper/conf/zoo.cfg

  sudo rm -f /var/zookeeper/data/myid
  sudo ln -s /var/local/deploy-dynamic/kafka/myid /var/zookeeper/data/myid

  echo "Starting ZooKeeper..."
  sudo service zookeeper restart
  echo "Zookeeper running!"
fi

