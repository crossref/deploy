# Terraform deployment for Hetzner Cloud.

terraform {
  backend "s3" {
    bucket = "event-data-deploy"
    key    = "terraform-state-hetzner"
    region = "eu-west-1"
  }
}

# Sets up:

# - A leader Docker Swarm node, with token and configuration.
# - A configurable number of follower Docker Swarm nodes.
# - A configurable number of ElasticSearch nodes.

# The Docker Swarm leader is the Docker Swarm manager node, and is
# used to generate the keys first time round. In addition, its IP
# address is the one used to connect to and deploy the docker yml
# file.

# Docker followers can be scaled up and down by adjusting the value of
# the docker_follower_count variable.

# NB various numbered docker nodes are given labels.
# Check null-resource.docker_deploy
variable docker_follower_count {
  default = 4
}

# Number of Kafka and ZooKeeper machines.
variable kafka_count {
  default = 3
}

# Elasticsearch nodes are all peers. The cluster can be scaled up and
# down by adjustin the value of elastic_count. Note that since no
# server is considered special, all Elasticsearch IP addresses are
# used in config files (e.g. logstash). 
variable elastic_count {
  default = 3
}

# The hetzner_cloud_token and ssh_public_key should be provided in a
# file called secrets.tsvars .

variable "hetzner_cloud_token" {}

variable "ssh_public_key" {}

# Configure the Hetzner Cloud Provider
provider "hcloud" {
  token = "${var.hetzner_cloud_token}"
}

resource "hcloud_ssh_key" "event_data_ssh_key" {
  name       = "event_data_ssh_key"
  public_key = "${var.ssh_public_key}"
}

# WARNING: Modifying this may result in virtual machines being deleted and re-created.
data "template_file" "cloudinit" {
  template = "${file("static/cloud-init.yml")}"

  vars = {
    authorized_key  = "${file("eventdatadeploy.pem.pub")}"
  }
}

## 
## BIG IMPORTANT NOTICE PLEASE
##
## DO NOT EDIT THESE hcloud_server RESOURCES LIGHTLY. A CHANGE IN THE
## WRONG PLACE CAN RESULT IN ALL MACHINES BEING DELETED AND RECREATED!
##
## ALWAYS RUN "TERRAFORM PLAN" BEFORE APPLY AND PAY CLOSE ATTENTION!
##

# Docker Leader virtual machine.
resource "hcloud_server" "docker_leader" {
  name        = "docker-leader"
  image       = "ubuntu-18.04"
  server_type = "cx51"
  ssh_keys    = ["${hcloud_ssh_key.event_data_ssh_key.id}"]
  user_data   = "${data.template_file.cloudinit.rendered}"
}

# Docker Follower virtual machines.
resource "hcloud_server" "docker_follower" {
  count       = "${var.docker_follower_count}"
  name        = "docker-follower-${count.index}"
  image       = "ubuntu-18.04"
  server_type = "cx51"
  ssh_keys    = ["${hcloud_ssh_key.event_data_ssh_key.id}"]
  user_data   = "${data.template_file.cloudinit.rendered}"
}

# ElasticSearch virtual machines.
resource "hcloud_server" "elastic" {
  count       = "${var.elastic_count}"
  name        = "elastic-${count.index}"
  image       = "ubuntu-18.04"
  server_type = "cx41"
  ssh_keys    = ["${hcloud_ssh_key.event_data_ssh_key.id}"]
  user_data   = "${data.template_file.cloudinit.rendered}"
}

# Kafka and ZooKeeper virtual machines.
resource "hcloud_server" "kafka" {
  count       = "${var.kafka_count}"
  name        = "kafka-${count.index}"
  image       = "ubuntu-18.04"
  server_type = "cx21"
  ssh_keys    = ["${hcloud_ssh_key.event_data_ssh_key.id}"]
  user_data   = "${data.template_file.cloudinit.rendered}"
}

resource "hcloud_volume" "kafka" {
  count       = "${var.kafka_count}"
  name = "kafka-${count.index}"
  size = 200
  server_id = "${element(hcloud_server.kafka.*.id, count.index)}"
  format = "ext4"
  automount = true
}

locals {
  all_machine_ips = "${concat(list(hcloud_server.docker_leader.ipv4_address), hcloud_server.docker_follower.*.ipv4_address, hcloud_server.elastic.*.ipv4_address,hcloud_server.kafka.*.ipv4_address)}"
  docker_machine_ips = "${concat(list(hcloud_server.docker_leader.ipv4_address), hcloud_server.docker_follower.*.ipv4_address)}"
}



# Update all machines' firewall config.
resource "null_resource" "network_connection_configuration" {
  count = "${length(local.all_machine_ips)}"

  # Rebuild whenever the list of IP addresses change (e.g. when adding
  # or removing a server).
  triggers = {
    ips = "${join(",", local.all_machine_ips)}"
  }

  depends_on = [
    "hcloud_server.docker_leader",
    "hcloud_server.docker_follower",
    "hcloud_server.elastic",
    "hcloud_server.kafka"
  ]

  connection {
    type        = "ssh"
    user        = "deploy"
    host        = "${element(local.all_machine_ips, count.index)}"
    port        = 22
    private_key = "${file("eventdatadeploy.pem")}"
  }

  # Set the timezone.
  provisioner "remote-exec" {
    inline = ["sudo ln -sf /usr/share/zoneinfo/UTC /etc/localtime"]
  }

  provisioner remote-exec {
    inline = [
      # Let the SSH-executor look up the address of the machine currently connecting to allow it in future.
      "`echo $SSH_CLIENT | awk '{print \"sudo ufw allow from \" $1 }'`",
      "sudo ufw default deny incoming",
      "sudo ufw allow from ${hcloud_server.docker_leader.ipv4_address}",
      "${join(" && ", formatlist("sudo ufw allow from %v", hcloud_server.docker_follower.*.ipv4_address))}",
      "sudo ufw --force enable",
      "sudo ufw reload"
    ]
  }
}

# Scripts to SSH into (and via) Docker leader.
resource "null_resource" "ssh-docker-leader" {
  triggers = {
    docker_leader   = "${hcloud_server.docker_leader.ipv4_address}"
  }

  depends_on = [
    "hcloud_server.docker_leader",
  ]

  provisioner "local-exec" {
    command = "mkdir -p templated-scripts"
  }

  # Template a local SOCKS proxy script that connects to an SSHD machine in the cluster.
  provisioner "local-exec" {
    command = <<EOF
echo '
# Connect to an SSHD running inside a container, so root user is fine.
ssh -N  -D 8123 -i eventdatadeploy.pem -p 2222 root@${hcloud_server.docker_leader.ipv4_address}
' > templated-scripts/run-socks.sh && chmod +x templated-scripts/run-socks.sh
EOF
  }

  # Template an SSH proxy script that connects to an SSHD machine INSIDE the Docker network.
  provisioner "local-exec" {
    command = <<EOF
echo '
# Connect to an SSHD running inside a container, so root user is fine.
ssh -i eventdatadeploy.pem -p 2222 root@${hcloud_server.docker_leader.ipv4_address}
' > templated-scripts/ssh-internal.sh && chmod +x templated-scripts/ssh-internal.sh
EOF
  }

  # Template an SSH proxy script that connects to the manager machine.
  provisioner "local-exec" {
    command = <<EOF
echo '
ssh -i eventdatadeploy.pem -p 22 deploy@${hcloud_server.docker_leader.ipv4_address}
' > templated-scripts/ssh-docker-leader.sh && chmod +x templated-scripts/ssh-docker-leader.sh
EOF
  }
}

# Template local scripts to SSH to docker follower machines.
resource "null_resource" "ssh-docker-follower" {
  count = "${var.docker_follower_count}"

  triggers = {
    docker_follower = "${join(",", hcloud_server.docker_follower.*.id)}"
  }

  depends_on = [
    "hcloud_server.docker_follower",
  ]

  provisioner "local-exec" {
    command = "mkdir -p templated"
  }

  # Template an SSH proxy script that connects to the manager machine.
  provisioner "local-exec" {
    command = <<EOF
echo '
ssh -i eventdatadeploy.pem -p 22 deploy@${element(hcloud_server.docker_follower.*.ipv4_address, count.index)}
' > templated-scripts/ssh-docker-follower-${count.index}.sh && chmod +x templated-scripts/ssh-docker-follower-${count.index}.sh
EOF
  }
}

# Create local scripts to SSH into Kafka machines.
resource "null_resource" "ssh-kafka" {
  count = "${var.kafka_count}"
  triggers = {
    kafka = "${join(",", hcloud_server.kafka.*.id)}"
  }

  depends_on = [
    "hcloud_server.kafka",
  ]

  provisioner "local-exec" {
    command = "mkdir -p templated-scripts"
  }

  # Template an SSH proxy script that connects to the manager machine.
  provisioner "local-exec" {
    command = <<EOF
echo '
ssh -i eventdatadeploy.pem -p 22 deploy@${element(hcloud_server.kafka.*.ipv4_address, count.index)}
' > templated-scripts/ssh-kafka-${count.index}.sh && chmod +x templated-scripts/ssh-kafka-${count.index}.sh
EOF
  }
}

# Create local scripts to SSH into Elastic Search machines.
resource "null_resource" "ssh-elastic" {
  count = "${var.elastic_count}"
  triggers = {
    elastic = "${join(",", hcloud_server.elastic.*.id)}"
  }

  depends_on = [
    "hcloud_server.elastic",
  ]

  provisioner "local-exec" {
    command = "mkdir -p templated"
  }

  # Template an SSH proxy script that connects to the manager machine.
  provisioner "local-exec" {
    command = <<EOF
echo '
ssh -i eventdatadeploy.pem -p 22 deploy@${element(hcloud_server.elastic.*.ipv4_address, count.index)}
' > templated-scripts/ssh-elastic-${count.index}.sh && chmod +x templated-scripts/ssh-elastic-${count.index}.sh
EOF
  }
}

# Configuration for Elastic Search machines.
# This can be sent before or after Elastic is installed.
data "template_file" "elasticsearch_yml" {
  template = "${file("static/elasticsearch/elasticsearch.yml.template")}"

  vars = {
    hosts  = "${jsonencode(hcloud_server.elastic.*.ipv4_address)}"
  }
}

resource "null_resource" "elastic_config" {
  count = "${var.elastic_count}"

  depends_on = [
    "null_resource.network_connection_configuration",
  ]

  # Reload if there's any change in the config, which includes changes in IP addresses.
  triggers = {
    elastic = "${data.template_file.elasticsearch_yml.rendered}"
  }

  connection {
    type        = "ssh"
    user        = "deploy"
    host        = "${element(hcloud_server.elastic.*.ipv4_address, count.index)}"
    port        = 22
    private_key = "${file("eventdatadeploy.pem")}"
  }

  # Need to ensure the full path exists otherwise get some inconsistent behaviour (filename is ignored in the file provisioner).
  provisioner "remote-exec" {
    inline = [
    "sudo mkdir -p /var/local/deploy-dynamic/elasticsearch",
    # And make sure we own it all.
    "sudo chown -R deploy /var/local/deploy-dynamic"]
  }

  # This will be symlinked in the Elastic install.
  provisioner "file" {
    content      = "${data.template_file.elasticsearch_yml.rendered}"
    destination = "/var/local/deploy-dynamic/elasticsearch/elasticsearch.yml"
  }

  # If ElasticSearch is running at this point, reload the config.
  # If not, don't. If this is the first time it will be reloaded anyway.
  # The random sleep is a bit hacky but it means they don't all get restarted at once.
  provisioner "remote-exec" {
    inline = ["sudo service --status-all | grep elasticsearch && sudo service elasticsearch restart"]


  }
}


# Configuration for Kafka machines. These run both Kafka and ZooKeeper.

data "template_file" "kafka_server_properties" {
  template = "${file("static/kafka/server.properties.template")}"
  count = "${var.kafka_count}"

  vars  = {
    broker_id = "${count.index}",
    # zookeeper_connect = "${data.template_file.zookeeper_connection_string.rendered}"
    zookeeper_connect = "${join(",", formatlist("%s:2181", hcloud_server.kafka.*.ipv4_address))}"
    advertised_listeners = "PLAINTEXT://${hcloud_server.kafka[count.index].ipv4_address}:9092"
  }
}

# Individual line from a ZooKeeper config.
data "template_file" "zookeeper_server" {
  count = "${var.kafka_count}"

  template = "server.$${i}=$${ip}:2888:3888"

  vars = {
    i = "${count.index}"
    ip = "${hcloud_server.kafka[count.index].ipv4_address}"
  }
}

# List of ZooKeeper servers, used by zookeeper.
data "template_file" "zookeeper_server_list" {
  template = "$${v}"

  vars = {
    v = "${join("\n", data.template_file.zookeeper_server.*.rendered)}"
  }
}

data "template_file" "zoo_cfg" {
  template = "${file("static/kafka/zoo.cfg.template")}"

  vars = {
    serverlist = "${join("\n", data.template_file.zookeeper_server_list.*.rendered)}"
  }
}

# Copy over config for ZooKeeper and kafka.
# This happens before, and could happen after, installation.
resource "null_resource" "kafka_config" {
  count = "${var.kafka_count}"

  depends_on = [
    "null_resource.network_connection_configuration",
  ]

  # Reload if there's any change in the config, which includes changes in IP addresses.
  triggers = {
    # Watch the ZooKeeper config. We can't watch a count-based resource, but the trigger
    # happens under the same circumstances.
    zookeeper = "${data.template_file.zoo_cfg.rendered}",

    # Of if the template changes.
    kafka = "${file("static/kafka/server.properties.template")}",
  }

  connection {
    type        = "ssh"
    user        = "deploy"
    host        = "${element(hcloud_server.kafka.*.ipv4_address, count.index)}"
    port        = 22
    private_key = "${file("eventdatadeploy.pem")}"
  }

  # Need to ensure the full path exists otherwise get some inconsistent behaviour (filename is ignored in the file provisioner).
  provisioner "remote-exec" {
    inline = [
      "sudo mkdir -p /var/local/deploy-dynamic/kafka",
      # And make sure we own it all.
      "sudo chown -R deploy /var/local/deploy-dynamic"
    ]
  }

  provisioner "file" {
    content      = "${data.template_file.kafka_server_properties[count.index].rendered}"
    destination = "/var/local/deploy-dynamic/kafka/server.properties"
  }

  provisioner "file" {
    content      = "${data.template_file.zoo_cfg.rendered}"
    destination = "/var/local/deploy-dynamic/kafka/zoo.cfg"
  }

  # Zookeeper wants this simple file to know which ID it has.
  # It will look this up in the zookeeper.properties file.
  provisioner "file" {
    content      = "${count.index}"
    destination = "/var/local/deploy-dynamic/kafka/myid"
  }

  # If Kafka and ZooKeeper are running at this point, reload the configs.
  # If not, don't. If this is the first time it will be reloaded anyway.
  # The random sleep is a bit hacky but it means they don't all get restarted at once.
  provisioner "remote-exec" {
    inline = [
    "if (sudo service --status-all | grep zookeeper) ; then sudo service zookeeper restart ; fi;",
    "if (sudo service --status-all | grep kafka) ; then sudo service kafka restart ; fi;"]
  }
}

# 
resource "null_resource" "kafka_install" {
  count = "${var.kafka_count}"

  depends_on = [
    "null_resource.kafka_config",
  ]

  # This should only be triggered when the VMs are created.
  # It doesn't mind running repeatedly.
  triggers = {
    ids = "${join(",", hcloud_server.kafka.*.id)}"
  }

  connection {
    type        = "ssh"
    user        = "deploy"
    host        = "${element(hcloud_server.kafka.*.ipv4_address, count.index)}"
    port        = 22
    private_key = "${file("eventdatadeploy.pem")}"
  }

  provisioner "remote-exec" {
    inline = [
        "sudo mkdir -p /mnt/kafka",
        "sudo chown deploy /mnt/kafka",
        # Only mount if not previously.
        "mountpoint /mnt/kafka || sudo mount -o discard,defaults ${element(hcloud_volume.kafka.*.linux_device, count.index)} /mnt/kafka",
        "sudo mkdir -p /mnt/kafka/logs",
        "sudo chown deploy /mnt/kafka"
      ]
  }

  provisioner "remote-exec" {
    scripts = [
      "static/install-zookeeper.sh",
      "static/install-kafka.sh",
    ]
  }
}

# All Docker machines get the same "foundation" setup.
# This applies both to the Leader and Followers, but the  Leader
# has a few extra bits to establish the Swarm config.
# This can safely be run repeatedly e.g. every time a new machine is added.

# Install docker on all docker machines.
resource "null_resource" "docker_install" {
  count = "${length(local.docker_machine_ips)}"

  depends_on = [
    "null_resource.network_connection_configuration",
  ]

  triggers = {
    ips   = "${join(",", local.docker_machine_ips)}"
  }

  connection {
    type        = "ssh"
    user        = "deploy"
    host        = "${element(local.docker_machine_ips, count.index)}"
    port        = 22
    private_key = "${file("eventdatadeploy.pem")}"
  }

  # Install software.
  provisioner "remote-exec" {
    scripts = ["static/install-docker.sh"]
  }

  # Create directories that get could get mapped to Docker containers.
  provisioner "remote-exec" {
    scripts = ["static/create-mount-directories.sh"]
  }
}

# Connect to the manager and create the swarm.
resource "null_resource" "docker_swarm_init" {
  depends_on = [
    "null_resource.docker_install"
  ]

  connection {
    type        = "ssh"
    user        = "deploy"
    host        = "${hcloud_server.docker_leader.ipv4_address}"
    port        = 22
    private_key = "${file("eventdatadeploy.pem")}"
  }

  # Set up Docker Swarm cluster if it needs doing.
  # We'll get an error if we're re-initing, in which case just ignore
  # and carry on.
  provisioner "remote-exec" {
    inline = ["sudo docker swarm init || true"]
  }

  # Create an overlay network that we can access.
  provisioner "remote-exec" {
    inline = ["sudo docker network create --driver overlay  --attachable --subnet 192.168.0.0/16 custom_attach_overlay || true"]
  }
}

# Connect to the leader and retrieve the token.
data "external" "docker_swarm_token" {
  program = ["env", "LEADER_ADDRESS=${hcloud_server.docker_leader.ipv4_address}", "./get-token.sh"]
}

# Join all workers to the swarm.
resource "null_resource" "docker_swarm_join" {
  count = "${var.docker_follower_count}"

  depends_on = [
    "null_resource.docker_swarm_init"
  ]

  triggers = {
    # If any are added, re-trigger.
    ips   = "${join(",", hcloud_server.docker_follower.*.ipv4_address)}",

    # Of if the token somehow changes.
    token = "${data.external.docker_swarm_token.result.token}"
  }

  connection {
    type        = "ssh"
    user        = "deploy"
    host        = "${element(hcloud_server.docker_follower.*.ipv4_address, count.index)}"
    port        = 22
    private_key = "${file("eventdatadeploy.pem")}"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo docker swarm join --token '${trimspace(data.external.docker_swarm_token.result.token)}' ${hcloud_server.docker_leader.ipv4_address}:2377 || true"
    ]
  }
}

## All the files that are passed into Docker containers.

# Logstash pipeline configured to send to Elastic Search machines.
data "template_file" "logstash_pipeline" {
  template = "${file("templates/logstash/pipeline.conf.template")}"

  vars = {
    addresses  = "${jsonencode(hcloud_server.elastic.*.ipv4_address)}"
  }
}

# Docker ENV file for referring to ElasticSearch machines.
data "template_file" "elastic_docker_env" {
  template = "${file("templates/docker/elasticsearch.env.template")}"

  # At the moment the client is only able to connect to one machine, so take the first.
  vars = {
    first_address  = "${element(hcloud_server.elastic.*.ipv4_address, 0)}"
  }
}

# Docker ENV file for referring to Kafka machines.
data "template_file" "kafka_docker_env" {
  template = "${file("templates/docker/kafka.env.template")}"

  # At the moment the client is only able to connect to one machine, so take the first.
  vars = {
    addresses = "${join(",", formatlist("%s:9092", hcloud_server.kafka.*.ipv4_address))}"
  }
}


# Configuration that's passed in to docker services.
resource "null_resource" "docker_services_configs" {
  count = "${length(local.docker_machine_ips)}"

  depends_on = [
    "null_resource.docker_swarm_init",
  ]

  triggers = {
    # Deploy to any change in IPs.
    ips   = "${join(",", local.docker_machine_ips)}",

    # Hash these to keep the state smaller and easier to compare.
    # Especially secrets should be hashed or they'll be saved on S3!
    logstash_pipeline = "${sha1(data.template_file.logstash_pipeline.rendered)}",

    # backend = "${sha1(file("static/docker/backend.yml"))}",
    haproxy = "${sha1(file("static/docker/haproxy.cfg"))}",
    config = "${sha1(file("static/docker/config.env"))}",

  }

  connection {
    type        = "ssh"
    user        = "deploy"
    host        = "${element(local.docker_machine_ips, count.index)}"
    port        = 22
    private_key = "${file("eventdatadeploy.pem")}"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mkdir -p /var/local/deploy-dynamic/docker",
      "sudo chown -R deploy /var/local/deploy-dynamic/docker",

      
      # Logstash wants its own directory.
      "mkdir -p /var/local/deploy-dynamic/docker/logstash/pipeline/",

      # Redis too.
      "mkdir /var/local/container-data/redis",
      "sudo chown -R deploy /var/local/container-data/redis",
    ]
  }

  provisioner "file" {
    content      = "${data.template_file.logstash_pipeline.rendered}"
    destination = "/var/local/deploy-dynamic/docker/logstash/pipeline/pipeline.conf"
  }

  provisioner "file" {
    source      = "static/docker/haproxy.cfg"
    destination = "/var/local/deploy-dynamic/docker/haproxy.cfg"
  }

  provisioner "file" {
    source = "static/docker/eventdata_tls.pem"
    destination = "/var/local/deploy-dynamic/docker/eventdata_tls.pem" 
  }
}

# Create all necessary Kafka topics.
resource "null_resource" "kafka_topics" {
  depends_on = [
    "null_resource.kafka_install"
  ]

  # Watch for the content of the file to change.
  triggers = {
    # Trigger if any file changes in the script.
    static = "${sha1(file("static/kafka/create-kafka-topics.sh"))}"
  }

  # Connect to the first machine, it will apply to the whole cluster.
  connection {
    type        = "ssh"
    user        = "deploy"
    host        = "${element(hcloud_server.kafka.*.ipv4_address, 0)}"
    port        = 22
    private_key = "${file("eventdatadeploy.pem")}"
  }

  # We'll get an error if we're re-initing, in which case just ignore
  # and carry on.
  provisioner "remote-exec" {
    scripts = ["static/kafka/create-kafka-topics.sh"]
  }
}

# Deploy Docker.

# Configuration that's passed in to docker services.
resource "null_resource" "docker_deploy" {
  depends_on = [
    "null_resource.docker_services_configs",
  ]

  triggers = {
    backend = "${sha1(file("static/docker/backend.yml"))}",

    elastic_docker_env = "${sha1(data.template_file.elastic_docker_env.rendered)}",
    kafka_docker_env = "${sha1(data.template_file.kafka_docker_env.rendered)}",

    # Important secrets are hashed.
    secrets = "${sha1(sha1(file("static/docker/secrets.env")))}"
    tls_pem = "${sha1(sha1(file("static/docker/eventdata_tls.pem")))}",

  }

  connection {
    type        = "ssh"
    user        = "deploy"
    host        = "${hcloud_server.docker_leader.ipv4_address}"
    port        = 22
    private_key = "${file("eventdatadeploy.pem")}"
  }


  provisioner "file" {
    content      = "${data.template_file.elastic_docker_env.rendered}"
    destination = "/var/local/deploy-dynamic/docker/elasticsearch.env"
  }

  provisioner "file" {
    content      = "${data.template_file.kafka_docker_env.rendered}"
    destination = "/var/local/deploy-dynamic/docker/kafka.env"
  }

  provisioner "file" {
    source      = "static/docker/backend.yml"
    destination = "/var/local/deploy-dynamic/docker/backend.yml"
  }

  provisioner "file" {
    source      = "static/docker/config.env"
    destination = "/var/local/deploy-dynamic/docker/config.env"
  }

  provisioner "file" {
    source      = "static/docker/secrets.env"
    destination = "/var/local/deploy-dynamic/docker/secrets.env"
  }

  # Roles for Docker machines, so we can make sure services are provisioned
  # sensibly. Not all services are constrained to these labels.
  # These are defined in this Terraform file because that's where the VMs are defined.
  provisioner "remote-exec" {
    inline = [

    # Leader - There is only one of these. Runs orchestration and light reporting duties.
    "sudo docker node update --label-add storage-role=leader docker-leader",

    # Zone 1, 2 and 3. For running Kafka and ZooKeeper nodes separately.
    # Don't run anything else disk-heavy on these, as they need a fixed amount of space.
    "sudo docker node update --label-add zone=1 docker-follower-0",
    "sudo docker node update --label-add zone=2 docker-follower-1",
    "sudo docker node update --label-add zone=3 docker-follower-2",

    # Snapshots require disk space on a regular basis.
    "sudo docker node update --label-add storage-role=snapshot docker-follower-3",

    # Storage required, e.g. Redis
    "sudo docker node update --label-add storage-role=redis docker-follower-2"
  ]
}

  # Install software.
  provisioner "remote-exec" {
    inline = ["sudo docker stack deploy --resolve-image changed --compose-file /var/local/deploy-dynamic/docker/backend.yml backend"]
  }
}
