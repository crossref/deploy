# Connect to docker leader, retreieve the swarm token.
ssh -o "StrictHostKeyChecking no" -i eventdatadeploy.pem deploy@$LEADER_ADDRESS 'sudo docker swarm join-token worker -q' | echo "{\"token\": \"`cat`\"}"

