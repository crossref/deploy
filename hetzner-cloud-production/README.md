# Terraform deployment for Hetzner Cloud.

The Event Data Agents (i.e. data collection and processing) run in Hetnzer Cloud, using Docker Swarm.

This README contains background information. The deploy.tf file is reasonably literate and explains things as it goes.

The following software is used in support:

 - Docker Swarm
 - Redis
 - Elastic Search
 - Kibana, Logstash, Logspout
 - Kafka
 - ZooKeeper
 - HAProxy

All virtual machines run either Docker, Elastic Search or Kafka & ZooKeeper. The other software runs within Docker. ElasticSearch doesn't run in Docker for technical reasons. 

## Connection to the outside world.

Docker Swarm manipulates iptables to perform its virtual networking. This means that open ports are all-or-nothing. There's currently no way of, for example, whitelisting IP addresses for certain services.

The following services are exposed to the public:

 - event-data-live-demo

These are served up by HAProxy from within the cluster.

The following services exist but are not exposed to the public:

 - Kibana

These must be accessed using an SSH SOCKS proxy. A container runs in the cluster, allowing you to connect and open a connection within the Docker virtual network. A script is created in `data/run-socks.sh` which will run a proxy locally. You should adjust your HTTP proxy rules to use localhost, then connect to http://kibana:5601

## Elastic Search and Docker

After a bit of investigation, it seems that Elastic Search isn't currently suitable for running in production on Docker Swarm. The ulimits configuration, though present in Docker Compose and Docker Engine, is missing from Docker Stack. This means that we can't provide the recommended deployment environment.

## Directory structures

### static

These are files that get copied to the remote machines. They are in source control but don't change at run-time.

### templates

Contains files that are templated by Terraform. The result of templating these are usually uploaded directly to virtual machines by Terraform.

### docker

The `docker` directory contains YML file that is executed with `docker stack` to deploy to the Docker Swarm cluster. This is checked in source control. This means that the current versions of all running software are known and documented.

### Directory mounts

Any Dockerized service could run on any Docker host, and might move from one host to another. The `data` directory contains configuration files, some of which are templated by Terraform. 

# To run

## First time 

1. Add `eventdatadeploy.pem` private key for SSH connection between you and the servers. Terraform will upload this key to Hetzner and it will automatically distributed to all virtual machines on creation.
2. Add `static/eventdata_tls.pem` for HAProxy to serve up TLS / HTTPS.
3. Add `static/docker/secrets.env`, based on `static/docker/secrets.env.example`. This is passed into Docker containers' environments. It should *not* be checked into source control.
4. Create `secrets.tvars`, based on `secrets.tvars.example`. This is used by Terraform. It should have keys:
   - `hetzner_cloud_token` (from the Hetzner Cloud console)
   - `ssh_public_key` the public key stored in Hetzner cloud. Corresponds to `eventdatadeploy.pem`.

## Ongoing basis

Note that terraform saves state, which is stored in S3. 

Terraform does not run automatically. Whenever you make a change (or the first time you run it), you need to trigger it. When this happens, it will compare the existing deployment to your deploy.tf file, and update what's deployed. **It is very important to preview changes before applying them. Some changes may result in virtual machines being deleted and potential data loss.**

Terraform monitors changes to the various files in `static`, incliuding the Docker Stack file at `static/docker/backend.yml`. Most day-to-day changes involve updating this file to run new versions of the the code. 

To apply changes, either first time or subsequent times:

1. Make your change in `static/docker/backend.yml` or in `deploy.tf`.
2. To see what Terraform thinks it will change, run: `terraform plan -var-file=secrets.tfvars`.
4. Read the output carefully. If it looks right, run: `terraform apply -var-file=secrets.tfvars`
5. Commit changes into git, explaining what changed, and push to GitHub.

For more info, read the [Terraform documentation](https://www.terraform.io/docs/). The `deploy.tf` is liberally sprinkled with comments.


## Tricks

From local machine, restart a particular service:

  ssh -i eventdatadeploy.pem -p 22 deploy@159.69.32.143 <<< docker service update --force «SERVICE_NAME»

REPL in e.g. investigator image

  sudo docker run -it --network custom_attach_overlay --env-file /var/local/deploy/static/docker/config.env --env-file /var/local/deploy/static/docker/secrets.env crossref/event-data-investigator lein repl

